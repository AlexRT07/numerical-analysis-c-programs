/*
* Programa que calcula la condicion de una matriz
* cond(A) = norm(A)*norm(A^-1)
*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void fillMatrix(double * A)
{
	A[0] = 20.0;
	A[1] = 15.0;
	A[2] = 10.0;
	A[3] = -3.0;
	A[4] = -2.249;
	A[5] = 7.0;
	A[6] = 5.0;
	A[7] = 1.0;
	A[8] = 3.0;
}

void fillVector(double * v)
{
	v[0] = 45.0;
	v[1] = 1.751;
	v[2] = 9.0;
}

void imprimeElemento(int nfilas, int ncols, double * A)
{
	int i;
	int j;
	for(i = 0; i < nfilas; i++)
	{
		for(j = 0; j < ncols; j++)
		{
			printf("%f \t", A[ncols*i + j]);
		}
		printf("\n");
	}
	printf("\n\n");
}

double norma(int nfilas, int ncols, double * A)
{
	/*Devuelve la norma 2 del elemento A*/
	int i,j;
	double sum = 0.0;
	for(i = 0; i < nfilas; i++)
	{
		for(j = 0; j < ncols; j++)
		{
			sum += A[ncols*i + j] * A[ncols*i + j];
		}
	}
	return sqrt(sum);
}

int main()
{
	double * A = NULL;
	A = malloc(3*3*sizeof(double));
	if(A == NULL)
	{
		printf("Error de Malloc\n");
		return -1;
	}
	fillMatrix(A);
	printf("La norma de A es: %f\n", norma(3,3,A));
	/*TODO: Calcular la inversa de A*/
	return 0;
}
