#include<stdio.h>

#define SIZE 3

void printMatrix(double * m, int rows, int cols)
{
	int i,j;
	for(i = 0; i < rows; i++)
	{
		for(j = 0; j < cols; j++)
		{
			printf("%f\t", m[cols*i + j]);
		}
		printf("\n");
	}
	printf("\n");
	printf("\n");
}

void lu(double * A, double * L, double * U)
{
	double factor;
	int i, j, k, cont = 1;
	
	for(i = 0; i < SIZE; i++)
	{
		for(j = 0; j < SIZE; j++)
		{
			U[SIZE * i + j] = A[SIZE * i + j];
		}
	}
	
	for(i = 0; i < SIZE; i++)
	{
		for(j = i; j < SIZE; j++)
		{
			if(j!=i)
			{
				factor = U[(SIZE) * j + i] / U[(SIZE) * i + i]; /*Cálculo del factor*/
				L[(SIZE) * j + i] = factor;
				for(k = 0; k < SIZE; k++) /*Transformación lineal de la fila correspondiente*/
				{
					U[(SIZE) * j + k] -= factor * U[(SIZE) * (j - cont) + k];
				}
				cont++;
			}
			else
			{
				L[(SIZE) * j + i] = 1;
			}
		}
		cont = 1;
	}
}

int main(int argc, char** argv)
{
	/*double A[SIZE][SIZE] = {{ 5.0, -2.0,  5.0, -5.0,  6.0, -8.0}, 
													{-6.0,  6.0, -3.0,  1.0,  4.0, -8.0}, 
													{-5.0,  5.0, -7.0,  3.0, -4.0,  3.0},
													{-1.0, -4.0, -9.0,  6.0,  9.0,  5.0},
													{ 7.0,  5.0, -8.0, -4.0,  7.0, -6.0},
													{ 4.0, -1.0, -3.0,  5.0, -3.0, -5.0}};*/
	double A[SIZE][SIZE] = {{1.0, 2.0, 4.0},
	                        {3.0, 8.0, 14.0},
	                        {2.0, 6.0, 13.0}};
	double L[SIZE][SIZE];
	double U[SIZE][SIZE];
	
	printf("La matriz A es:\n");
	printMatrix(&A[0][0], SIZE, SIZE);
	
	/*Build L and U matrix via Forward Elimination*/
	lu(&A[0][0], &L[0][0], &U[0][0]);
	printf("La matriz L es:\n");
	printMatrix(&L[0][0], SIZE, SIZE);
	printf("La matriz U es:\n");
	printMatrix(&U[0][0], SIZE, SIZE);

	return 0;
}
