#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define NROWS 4
#define NCOLS 3

double dot(double * v1, double * v2)
{
	int i;
	double p = 0.0;
	for(i = 0; i < NROWS; i++)
	{
		p += v1[i] * v2[i];
	}
	return p;
}

void getColFromMatrix(int c, double * A, double * v)
{
	int i;
	for(i = 0; i < NROWS; i++)
	{
		v[i] = A[i * NCOLS + c];
	}
}

double norm(double * v, int nrows)
{
	int i;
	double norm = 0.0;
	for(i = 0; i < nrows; i++)
	{
		norm += v[i]*v[i];
	}
	return sqrt(norm);
}

void scalarTimesMatrix(double scalar, double * M, double * Out, int rows, int cols)
{
	int i,j;
	for(i = 0; i < rows; i++)
	{
		for(j = 0; j < cols; j++)
		{
			Out[cols * i + j] = scalar * M[cols * i + j];
		}
	}
}

void transpose(double * M, double * Out, int rows, int cols)
{
	int i,j;
	for(i = 0; i < rows; i++)
	{
		for(j = 0; j < cols; j++)
		{
			Out[rows * j + i] = M[cols * i + j];
		}
	}
}

void printMatrix(double * m, int rows, int cols)
{
	int i,j;
	for(i = 0; i < rows; i++)
	{
		for(j = 0; j < cols; j++)
		{
			printf("%f\t", m[cols*i + j]);
		}
		printf("\n");
	}
	printf("\n");
	printf("\n");
}

void matrixMultiplication(double * M1, double * M2, double * Out, 
                          int rows1, int cols1, int rows2, int cols2)
{
	int i,j,k;
	if(cols1 != rows2)
	{
		printf("La multiplicación no puede ser realizada.\n");
		exit(-1);
	}
	for(i = 0; i < rows1; i++)
	{
		for(j = 0; j < cols2; j++)
		{
			Out[cols2 * i + j] = 0.0;
			for(k = 0; k < cols1; k++)
			{
				Out[cols2 * i + j] += M1[cols1 * i + k] * M2[cols2 * k + j];
			}
		}
	}
}

void setColumnInMatrix(int nCol, double * v, double * M, int nrows, int ncols)
{
	int i;
	for(i = 0; i < nrows; i++)
	{
		M[ncols * i + nCol] = v[i];
	}
}

void sumMatrix(double * M1, double * M2, double * Out, int nrows, int ncols)
{
	int i,j;
	for(i = 0; i < nrows; i++)
	{
		for(j = 0; j < ncols; j++)
		{
			Out[ncols * i + j] = M2[ncols * i + j] + M1[ncols * i + j];
		}
	}
}

double lambda(double * v, double * a, int nrows)
{
	return dot(v,a)/dot(v,v);
}

int main(int argc, char** argv)
{
	double A[NROWS][NCOLS] = {{-1.0,-1.0,1.0}, {1.0,3.0,3.0}, {-1.0,-1.0,5.0}, {1.0,3.0,7.0}};
	double Q[NROWS][NCOLS];
	double QT[NCOLS][NROWS];
	double R[NCOLS][NCOLS];
	double q1[NROWS], q2[NROWS], q3[NROWS], Q1[NROWS], Q2[NROWS], Q3[NROWS], temp[NROWS];
	
	printf("La Matriz A es:\n");
	printMatrix(&A[0][0], NROWS, NCOLS);

	/*getColFromMatrix(0, &A[0][0], &q1[0]);	
	printf("La primera fila de A es:\n");
	printMatrix(&q1[0], NROWS, 1);
	
	transpose(&A[0][0], &Tp[0][0], NROWS, NCOLS);
	printf("La traspuesta de A es:\n");
	printMatrix(&Tp[0][0], NCOLS, NROWS);
	
	matrixMultiplication(&A[0][0], &Tp[0][0], &R[0][0], NROWS, NCOLS, NCOLS, NROWS);
	printf("Producto de A por su traspuesta es:\n");
	printMatrix(&R[0][0], NROWS, NROWS);*/
	
	getColFromMatrix(0, &A[0][0], &q1[0]);
	scalarTimesMatrix(1/norm(&q1[0], NROWS), &q1[0], &Q1[0], NROWS, 1);
	setColumnInMatrix(0, &Q1[0], &Q[0][0], NROWS, NCOLS);
	
	getColFromMatrix(1, &A[0][0], &q2[0]);
	scalarTimesMatrix(-1*lambda(&q1[0], &q2[0], NROWS), &q1[0], &Q2[0], NROWS, 1);
	sumMatrix(&q2[0], &Q2[0], &Q2[0], NROWS, 1);
	scalarTimesMatrix(1/norm(&Q2[0], NROWS), &Q2[0], &Q2[0], NROWS, 1);
	setColumnInMatrix(1, &Q2[0], &Q[0][0], NROWS, NCOLS);
	
	getColFromMatrix(2, &A[0][0], &q3[0]);
	scalarTimesMatrix(-1*lambda(&q1[0], &q3[0], NROWS), &q1[0], &Q3[0], NROWS, 1);
	scalarTimesMatrix(-1*lambda(&Q2[0], &q3[0], NROWS), &Q2[0], &temp[0], NROWS, 1);
	sumMatrix(&temp[0], &Q3[0], &Q3[0], NROWS, 1);
	sumMatrix(&q3[0], &Q3[0], &Q3[0], NROWS, 1);
	scalarTimesMatrix(1/norm(&Q3[0], NROWS), &Q3[0], &Q3[0], NROWS, 1);
	setColumnInMatrix(2, &Q3[0], &Q[0][0], NROWS, NCOLS);
	
	printf("La matriz Q es:\n");
	printMatrix(&Q[0][0], NROWS, NCOLS);
	
	transpose(&Q[0][0], &QT[0][0], NROWS, NCOLS);
	printf("La matriz Q traspuesta es:\n");
	printMatrix(&QT[0][0], NCOLS, NROWS);
	matrixMultiplication(&QT[0][0], &A[0][0], &R[0][0], NCOLS, NROWS, NROWS, NCOLS);
	
	printf("La matriz R es:\n");
	printMatrix(&R[0][0], NCOLS, NCOLS);
	return 0;
}
