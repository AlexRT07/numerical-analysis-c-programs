/*
 * Código en C del método de newton-raphson para una 
 * determinada función
 *
 * M.C.C. Alex A. Turriza Suárez 
 */

#include<stdio.h>
#include<stdlib.h>

double evaluacion(double x)
{
	return x*x*x - 0.165*x*x + 3.993e-4;
}

double derivada(double x)
{
	return 3.0*x*x - 2.0*0.165*x;
}

double nr(double xi)
{
	int i;
	double fxp, fxi, xin;
	for(i = 0; i < 100; i++)
	{
		fxi = evaluacion(xi);
		fxp = derivada(xi);
		xin = xi - fxi / fxp;
		xi = xin;
	}
	return xi;
}

int main(int argc, char ** argv)
{
	if(argc != 2)
	{
		printf("Uso: ./exe xi\n");
		return -1;
	}
	double xi = atof(argv[1]);
	double xr;
	xr = nr(xi);
	printf("La raiz es: %f\n", xr);
	return 0;
}
