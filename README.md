# Numerical-Analysis C Programs

These are some C codes of known methods used in some numerical problems in many areas. All codes are based in the 2021-3 course of Numerical Analysis and written by Alex Turriza-Suarez in the Aeronautical University on Queretaro UNAQ, Mexico.

## How to compile

Every code in this repo could be compiled as simple as a single gcc command line:

```
gcc -Wall -o <YourExecutableNameHere> code.c -lm
```

Or add to your IDE. Please note that the compiler would ask you to add the math library linker [-lm](https://man7.org/linux/man-pages/man0/math.h.0p.html) to the configuration of the compiler or the project. Although the code could compile without it, it's always a good practice to add the linker, as the documentation of C asks for it.

## Individual documentation

All codes have their own documentation (in spanish). The comments in some codes may be incomplete.
