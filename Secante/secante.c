/*
 * Código en C del método de la secante para una 
 * determinada función
 *
 * M.C.C. Alex A. Turriza Suárez 
 */

#include<stdio.h>
#include<stdlib.h>

double evaluacion(double x)
{
	return x*x*x - 0.165*x*x + 3.993e-4;
}

double secante(double xi, double h)
{
	int i;
	double xin;
	for(i = 0; i < 100; i++)
	{
		xin = xi - (evaluacion(xi) * h) / (evaluacion(xi + h) - evaluacion(xi));
		xi = xin;
	}
	return xi;
}

int main(int argc, char ** argv)
{
	if(argc != 3)
	{
		printf("Uso: ./exe xi h\n");
		return -1;
	}
	double xi = atof(argv[1]);
	double h = atof(argv[2]);
	double xr;
	xr = secante(xi, h);
	printf("La raiz es: %f\n", xr);
	return 0;
}
