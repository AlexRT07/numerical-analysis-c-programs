/*
 * Programa utilizado para codificar el algoritmo de Naive Gauss
 * Análisis Numérico
 * Alex A. Turriza Suárez
 */
 
#include<stdio.h>
#include<stdlib.h>

void fillMatrix(double * A)
{
	/*A[0] = 25;
	A[1] = 5;
	A[2] = 1;
	A[3] = 64;
	A[4] = 8;
	A[5] = 1;
	A[6] = 144;
	A[7] = 12;
	A[8] = 1;*/
	A[0] = 10;
	A[1] = -7;
	A[2] = 0;
	A[3] = -3;
	A[4] = 2.099;
	A[5] = 6;
	A[6] = 5;
	A[7] = -1;
	A[8] = 5;
}

void fillVector(double * v)
{
	/*v[0] = 106.8;
	v[1] = 177.2;
	v[2] = 279.2;*/
	v[0] = 7;
	v[1] = 3.901;
	v[2] = 6;
}

void imprimeElemento(int nfilas, int ncols, double * A)
{
	int i;
	int j;
	for(i = 0; i < nfilas; i++)
	{
		for(j = 0; j < ncols; j++)
		{
			printf("%f \t", A[ncols*i + j]);
		}
		printf("\n");
	}
	printf("\n\n");
}

double * creaMatrizAumentada(int dims, double * A, double * b)
{
	int i = 0;
	int j = 0;
	double * Ab = malloc(dims*(dims+1)*sizeof(double));
	if(Ab == NULL)
	{
		printf("Malloc: Error\n");
		exit(-1);
	}
	for(i = 0; i < dims; i++)
	{
		for(j = 0; j < dims + 1; j++)
		{
			if(j < dims)
			{
				Ab[(dims+1)*i + j] = A[dims*i + j];
			}
			else
				Ab[(dims+1)*i + j] = b[i];
		}
	}
	return Ab;
}

void aTriangularSuperior(int dims, double * Ab)
{
	int i = 0;
	int j = 0;
	int k = 0;
	double c;
	for(i = 0; i < dims - 1; i++)	
	{
		for(j = 1; j < dims; j++)
		{
			c = Ab[(i+j)*(dims+1) + i] / Ab[i*(dims+1) + i];
			for(k = 0; k < dims + 1; k++)
			{
				Ab[(i+j)*(dims+1) + k] = Ab[(i+j)*(dims+1) + k] - c * Ab[i*(dims+1) + k];
			}
		}
	}
}

double * sustitucionHaciaAtras(int dims, double * Ab)
{
	int i, j;
	double sum;
	double * x = malloc(dims*sizeof(double));
	if(x == NULL)
	{
		printf("Malloc ERROR\n");
		exit(-1);
	}
	x[dims - 1] = Ab[(dims+1)*(dims)-1] / Ab[(dims+1)*dims - 2];
	
	for(i = dims - 2; i >= 0; i--)
	{
		sum = 0.0;
		for(j = i + 1; j < dims; j++)
		{
			sum += x[j]*Ab[(dims+1)*i + j];
		}
		x[i] = (Ab[(dims+1)*(i+1) - 1] - sum) / Ab[(dims+1)*i + i];
	}
	return x;
}

int main()
{
	double * A = NULL;
	double * b = NULL;
	double * x = NULL;
	double * Ab = NULL;
	A = malloc(3*3*sizeof(double));
	b = malloc(3*sizeof(double));
	if(A == NULL || b == NULL)
	{
		printf("Malloc: Error\n");
		return -1;
	}
	fillMatrix(A);
	fillVector(b);
	Ab = creaMatrizAumentada(3, A, b);
	imprimeElemento(3,3,A);
	imprimeElemento(3,1,b);
	imprimeElemento(3,4,Ab);
	
	aTriangularSuperior(3, Ab);
	imprimeElemento(3,4,Ab);
	x = sustitucionHaciaAtras(3, Ab);
	printf("\nLa solucion del sistema es: \n");
	imprimeElemento(3,1,x);
	
	free(A);
	free(b);
	free(x);
	free(Ab);
	return 0;
}
