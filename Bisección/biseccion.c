/*
 * Código en C del método de bisección para una determinada 
 *función
 *
 * M.C.C. Alex A. Turriza Suárez 
 */

#include<stdio.h>
#include<stdlib.h>

double evaluacion(double x)
{
	/*Función de Ejemplo*/
	return x*x*x - 0.165*x*x + 3.993e-4;
}

double biseccion(double xl, double xu)
{
	/*Devuelve la raiz de la función por el método de bisección*/
	int i;
	double xm;
	double fxm, fxl, fxu;
	for(i = 0; i < 100; i++)
	{
		xm = (xl + xu) / 2.0;
		fxm = evaluacion(xm);
		fxl = evaluacion(xl);
		fxu = evaluacion(xu);
		if(fxm == 0.0 )
		{
			/*Raíz exacta encontrada*/
			return xm;
		}
		else if(fxm*fxu < 0.0)
		{
			/*Raíz encerrada entre xm y xu*/
			xl = xm;
		}
		else if(fxl*fxm < 0.0)
		{
			/*Raíz encerrada entre xl y xm*/
			xu = xm;
		}
		else
		{
			/*No existieron cambios de signo*/
			printf("Al parecer, no se encuentra una raíz en este intervalo\n");
			exit(-1);
		}
	}
	return xm;
}

int main(int argc, char ** argv)
{
	if(argc != 3)
	{
		printf("Uso: ./exe xl xu\n");
		return -1;
	}
	double xl = atof(argv[1]);
	double xu = atof(argv[2]);
	double xr;
	xr = biseccion(xl, xu);
	printf("La raiz es: %f\n", xr);
	return 0;
}
