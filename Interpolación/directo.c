/*
 * Metodo directo de interpolacion
 * 
 * M.C.C. Alex A. Turriza Suarez
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void inicializaTabla(double * td)
{
	td[0] = 0.0;
	td[1] = 0.0;
	td[2] = 10.0;
	td[3] = 227.04;
	td[4] = 15.0;
	td[5] = 362.78;
	td[6] = 20.0;
	td[7] = 517.35;
	td[8] = 22.5;
	td[9] = 602.97;
	td[10] = 30.0;
	td[11] = 901.67;
}

void imprimeTabla(double * td, int nEntradas)
{
	int i;
	for(i = 0; i<nEntradas; i++)
	{
		printf("%f \t %f\n", td[2*i], td[2*i+1]);
	}
}

void imprimeElemento(int nfilas, int ncols, double * A)
{
	int i;
	int j;
	for(i = 0; i < nfilas; i++)
	{
		for(j = 0; j < ncols; j++)
		{
			printf("%f \t", A[ncols*i + j]);
		}
		printf("\n");
	}
	printf("\n\n");
}


void buildCoeffMtx(int grado, double * td, double * coeff, int it)
{
	int i, j;
	/*coeff[0] = pow(td[2*(it) + 0], 0);
	coeff[1] = pow(td[2*(it) + 0], 1);
	coeff[2] = pow(td[2*(it) + 2], 0);
	coeff[3] = pow(td[2*(it) + 2], 1);*/
	
	for(i = 0; i < grado + 1; i++)
	{
		for(j = 0; j < grado + 1; j++)
		{
			coeff[(grado + 1)*i + j] = pow(td[2*(it) + 2*i], j);
		}
	}
}

void buildLDVector(int grado, double * td, double * ld, int it)
{
	int i;
	for(i = 0; i < grado + 1; i++)
	{
		ld[i] = td[(2*it + 1) + 2*i];
	}
}

double * interpolacionLineal(double * td, int nEntradas)
{
	/*
	* td es el apuntador a la tabla de datos
	* nEntradas es el numero de filas de la tabla td 
	*/
	double * constants = NULL;
	double * coeff = NULL;
	double * ld = NULL;
	int i;
	constants = malloc(2*(nEntradas - 1) * sizeof(double) );
	coeff = malloc(2*2 * sizeof(double) );
	ld = malloc(2 * sizeof(double));
	if(constants == NULL || coeff == NULL || ld == NULL)
	{
		printf("Error de asignacion de memoria en MALLOC\n");
		exit(-1);
	}
	for(i = 0; i < nEntradas - 1; i++)
	{
		buildCoeffMtx(1, td, coeff, i);
		buildLDVector(1, td, ld, i);
		imprimeElemento(2,2,coeff);
		imprimeElemento(2,1,ld);
	}
}

int main()
{
	double * td = NULL;
	
	td = malloc(6*2*sizeof(double));
	if(td == NULL)
	{
		printf("Problema con solicitud de memoria, MALLOC\n");
		return -1;
	}
	inicializaTabla(td);
	imprimeTabla(td,6);
	printf("\n\n");
	interpolacionLineal(td, 6);
	return 0;
}
