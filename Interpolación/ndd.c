#include<stdio.h>
#include<stdlib.h>

void inicializaTabla(double * td)
{
	td[0] = 0.0;
	td[1] = 0.0;
	td[2] = 10.0;
	td[3] = 227.04;
	td[4] = 15.0;
	td[5] = 362.78;
	td[6] = 20.0;
	td[7] = 517.35;
	td[8] = 22.5;
	td[9] = 602.97;
	td[10] = 30.0;
	td[11] = 901.67;
}

void imprimeElemento(int nfilas, int ncols, double * A)
{
	int i;
	int j;
	for(i = 0; i < nfilas; i++)
	{
		for(j = 0; j < ncols; j++)
		{
			printf("%f \t", A[ncols*i + j]);
		}
		printf("\n");
	}
	printf("\n\n");
}

double * interpolacion(int d, double *td)
{
	double * constants = NULL;
	constants = malloc(d+1 * sizeof(double));
	
}

int main()
{
	double * td = NULL;
	td = malloc(6*2*sizeof(double));
	if(td == NULL)
	{
		printf("Problema con solicitud de memoria, MALLOC\n");
		return -1;
	}
	inicializaTabla(td);
	imprimeElemento(6, 2, td);
	return 0;
}
